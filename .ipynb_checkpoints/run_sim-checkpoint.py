runfile('C:/Users/Amy.Cook/github/kalman_filters/kalman_filter.py', wdir='C:/Users/Amy.Cook/github/kalman_filters')

import matplotlib
from matplotlib import pyplot as plt

# run sim
A = np.array([[0, 1], [0, 0]]) # continuous time state transition matrix
B = np.array([[0],[1]]) # continuous time input model
H = np.array([[1, 0]]) # observation model
Q = np.array([[0.01]]) #continuous time noise variance
R = np.array([[1]]) #observation noise variance
x0 = np.array([[0],[0]]) # initial conditions

dt = 1.0 # sample period
t_steps = 500 # number of time-steps to simulate
time = np.arange(0, dt*t_steps, dt)
F = np.array([[1, dt], [0, 1]])
G = np.array([[dt**2/2], [dt]])
[z,x] = xtrue(F, G, H, Q, R, x0, t_steps)  # note the semi-colon

# plot first 50 tsteps
fig = plt.figure()
plt.plot(x[0,1:50])
plt.plot(z[0,1:50])
plt.show()


# run filter
# assumes run_sim has been run at least once
#Q = Q*10
#R = R*10
B = np.array([1.0])
Q_factor = 10
R_factor = 1
[W, Pest, Ppred, S, xest, xpred, xinnov] = lkf_amy(F, G, H, Q, R, x0, Q_factor, R_factor, t_steps, z)

# plot first n tsteps
ind1 = 490
ind2 = 500

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(xest[0, ind1:ind2], label='x_est')
ax.plot(z[0, ind1:ind2], label='z')
ax.plot(xpred[0, ind1:ind2], label='x_pred')
ax.plot(x[0,ind1:ind2], label='x_true')
ax.legend()
plt.show()


# have a look at the error
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(xest[0, :] - z[0,0:500], label='x_est - z')
ax.legend()
plt.show()


# have a look at W
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(W[:, 0], label='W')
ax.legend()
plt.show()

# have a look at velocity
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(W[:, 0], label='W')
ax.legend()
plt.show()







