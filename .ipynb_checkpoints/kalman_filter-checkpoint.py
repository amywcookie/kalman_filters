
import numpy as np

def xtrue(F,G,H,Q,R,x0,t_steps):
    '''
    Translated from Hugh Durrant-Whyte 7/1/94 Matlab code.

    A function to compute true state-space history
    and true observations from a discrete-time
    model with no input. For use with a Kalman Filter
    :param F: Xsize*Xsize state transition matrix
    :param G: Xsize*Vsize state noise transition matrix
    :param H: Zsize*Xsize observation matrix
    :param Q: Vsize*Vsize process noise covariance matrix
    :param R: Zsize*Zsize observation noise covariance matrix
    :param x0: Xsize*1 initial state vector
    :param t_steps: number of time-steps to be simulated
    :return: z: Zsize*t_steps Observation time history
             x: Xsize*t_steps true state time history

    '''

    # First check all matrix dimensions
    #*************************************************
    FRows, FCols = F.shape
    if FRows != FCols:
        raise Exception('F is non-square')

    Xsize, nul = x0.shape
    if Xsize != FRows:
      raise Exception('x0 does not match dimension of F')

    GRows, Vsize = G.shape
    if Xsize != GRows:
      raise Exception('G does not match dimension of F')

    QRows, QCols = Q.shape
    if QRows != QCols:
      raise Exception('Q must be square')

    if Vsize != QRows:
      raise Exception('Q does not match dimension of G')

    Zsize, HCols = H.shape
    if HCols != Xsize:
      raise Exception('H and Xsize do not match')

    RRows, RCols = R.shape
    if RRows != RCols:
      raise Exception('R must be square')

    if RRows != Zsize:
      raise Exception('R must match Zsize of H')

    #***********************************************************
    # end checking of dimensions
    
    # now actual code

    # intialise output matricies
    x = np.zeros((Xsize, t_steps + 1))
    z = np.zeros((Zsize, t_steps + 1))

    # get some gaussian noise
    # rand('normal')
    v = np.sqrt(Q) @ np.random.normal(0, 1, (Vsize, t_steps+1))
    w = np.sqrt(R) @ np.random.normal(0, 1, (Zsize, t_steps+1))

    # initial value
    x[:,0, None] = x0

    # now generate all the remaining states
    for i in range(t_steps):
       x[:, i+1] = F @ x[:,i] + G @ v[:,i]

    # then all the observations
    for i in range(t_steps + 1):
       z[:, i] = H @ x[:,i] + w[:,i]

    return([z, x])

def lkf_amy(F, G, H, Q, R, x0, Q_factor, R_factor, t_steps, z):
    '''
    Hugh Durrant-Whyte 7/1/94.
    A function to compute gain and covariance history for a Kalman Filter
    :param F: Xsize*Xsize state transition matrix
    :param G: Xsize*Vsize state noise transition matrix
    :param H: Zsize*Xsize observation matrix
    :param Q: Vsize*Vsize process noise covariance matrix
    :param R: Zsize*Zsize observation noise covariance matrix
    :param Q_factor: multiplier for Q
    :param t_steps: number of time-steps to be simulated
    :return: [W, Pest, Ppred, S] which are the Gain history,
                Estimate Covariance history,
                Prediction Covariance history,
                Innovation Covariance history
    '''

    # First check all matrix dimensions

    Xsize, Cols = F.shape
    if Xsize != Cols:
       raise Exception('F is non-square')

    Rows,Vsize = G.shape
    if Xsize != Rows:
      raise Exception('G does not match dimension of F')

    Rows,Cols = Q.shape
    if Rows != Cols:
      raise Exception('Q must be square')

    if Vsize != Rows:
      raise Exception('Q does not match dimension of G')

    Zsize, Cols = H.shape
    if Cols != Xsize:
      raise Exception('H and Xsize do not match')

    Rows,Cols = R.shape
    if Rows != Cols:
      raise Exception('R must be square')

    if Rows != Zsize:
      raise Exception('R must match Zsize of H')

    # end checking of dimensions

    # fix up output matricies
    W = np.zeros((t_steps, Xsize*Zsize))
    Pest = np.zeros((t_steps, Xsize, Xsize))
    Ppred = np.zeros((t_steps, Xsize, Xsize))
    S = np.zeros((t_steps, Zsize*Zsize))
    xest = np.zeros((Xsize, t_steps))
    xpred = np.zeros((Xsize, t_steps))
    innov = np.zeros((Zsize, t_steps))

    Qm = G @ Q @ G.T
    Rm = B @ R @ B.T
    Rm = R_factor * Rm

    # initial value
    Pest[0,:,:] = Qm
    xpred[:, 0, None] = F @ x0
    innov[:, 0, None] = z[:, 0] - H @ xpred[:, 0]

    # ready to go !
    for i in range(t_steps-1):
       #print(i)
       # first the actual calculation in local variables
       Ppred[i+1] = F @ Pest[i] @ F.T + G @ Q @ G.T
       # S is the innovation covariance
       lS = H @ Ppred[i+1] @ H.T + Rm
       lW = Ppred[i+1] @ H.T @ np.linalg.inv(lS)
       # subtract the gain ^2 *
       Pest[i + 1] = Ppred[i+1] - lW @ lS @ lW.T
       # then record the results in columns of output states
       W[i + 1] = lW.reshape(1, Xsize*Zsize)
       S[i + 1] = lS.reshape(1, Zsize*Zsize)

       #estimates
       xpred[:, i + 1] = F @ xest[:, i]
       innov[:, i + 1] = z[:, i + 1] - H @ xpred[:, i + 1]
       xest[:, i + 1] = xpred[:, i + 1] + lW @ innov[:, i + 1]

    return([W,Pest,Ppred,S, xest, xpred, innov])


def pc_in_innov(xinnov, S):
    """
    A functinn to calculate the percentage of innovations from each time step that are within one standard deviation of the
    calculated innovation standard deviation (sqrt of S)
    :param xinnov: Zsize*tsteps innovation matrix = xpred - z
    :param S: t_steps*Zsize innovation variance matrix = P + R
    """
    
    bool_within = [(xinnov[0,i] < np.sqrt(S[i,0])) and (xinnov[0,i] > -np.sqrt(S[i,0])) for i, inno in enumerate(xinnov[0,:])]
    pc_within = np.sum(bool_within) / xinnov.shape[1]
    
    return(pc_within)
    
    
def auto(x):
    """
    Rewrite of Hugh Durrant-Whyte 5-Jan-94
    Computes autocorrelation of input data set. N is the number of 
    data points, x is a column matrix holding the input data set.
    Uses fft method as advertised in Maybeck p193.
    """

    # first find the size of input vector
    [N,nul]=size(x') 
    M=round(N/2)

    # then compute PSD of data 
    X = fft(x)
    Pxx = X.*conj(X)/N

    # then inverse is autocorrelation
    Rxx = real(ifft(Pxx))
    Rxx = Rxx(1:M)
    fact = Rxx(1)
    for i = 1:M
       Rxx(i) = Rxx(i)/fact
                 
    return(Rxx)

                 



