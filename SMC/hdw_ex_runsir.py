# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:42:10 2020

@author: Amy.Cook
"""

import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from functions_sir import *


# now set up and run sir algorithm

NUM_SAMP = 500
SIM_LENGTH = 100

# initialise with state samples uniformly distributed on interval +/-25
xinit = 50*(sorteduniform(NUM_SAMP)-0.5)
winit = 1/NUM_SAMP # and weights uniformly distributed
winit = winit*np.ones((NUM_SAMP,1))

# now run the SIR algorithm
xdata, zdata = gen_data(NUM_SAMP, SIM_LENGTH)
#plt.plot(xdata[0:100])
#plt.plot(zdata[0:100])

xtrack, wtrack = sir(xinit, winit, zdata)

# make plot
xstime,pxsdata,xsmean = create_plot_data(SIM_LENGTH, NUM_SAMP, xtrack)
time = np.arange(0,SIM_LENGTH,1)


min_i = 0
max_i = 20

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(xstime[(xstime<max_i) & (xstime > min_i)], pxsdata[(xstime<max_i) & (xstime > min_i)], 'g.', label='x_samples')
ax.plot(time[min_i:max_i], xsmean[0, min_i:max_i], 'r', label='x_mean')
ax.plot(time[min_i:max_i], xdata[min_i:max_i,0], 'b', label='x_true')
ax.plot(time[min_i:max_i], zdata[min_i:max_i,0], 'mx', label='obs')
ax.legend()
plt.show()




