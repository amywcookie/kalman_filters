# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:44:42 2020

@author: Amy.Cook
"""

import numpy as np

def sorteduniform(n):
    # an algorithm to compute an ordered set of uniformly 
    # distributed random numbers
    
    u = np.zeros((n,1)) # a column
    c= 1/n
    c2= c/2
    u[0] = c2
    ur = c2*(np.random.uniform(size = (u.shape)) - 0.5)
    
    for i in range(1, n):
       u[i] = u[i-1] + c
       u[i-1] = u[i-1] + ur[i-1]

    u[n-1] = u[n-1] + ur[n-1]
    
    return(u)
    

def gen_data(NUM_SAMP=500, SIM_LENGTH=100):   
    # test of SIR algorithm
    # this uses a generic sir algorithm, but the test is
    # set up for a simple scalar example
    
    # something to generate true path and observations
    xdata = np.zeros((SIM_LENGTH,1))
    zdata = np.zeros((SIM_LENGTH,1))
    
    for k in range(1, SIM_LENGTH):
       xdata[k] = prediction(xdata[k-1], k)
       
    zdata = np.array([observe(x)[0] for x in xdata])[:,None]
    
    return(xdata, zdata)
       
       
def prediction(xe,k, Q=10):
    '''
    % HDW 25/02/2003
    %
    % this is a simple function that implements a prediction model
    % it does the obvious thing: Takes a set of samples xe at time k
    % and passes them, one by one, through the prediction model
    # It adds some noise (part of the model) each time through
    # Output is a set of samples xp.
    '''
    
    # this is the nominal process model
    xp = (xe/2) + (25*xe)/(1+(xe*xe)) + 8*np.cos(1.2*k)
    
    # this is the process noise term
    q= np.sqrt(Q)*np.random.normal(0, 1, xe.shape)
    
    # noise is additive in this case
    xp = xp+q
    return(xp)
    
def observe(x, R = 1):
    # function to implement observation
    
    nx = len(x)
    z = np.zeros(x.shape)
    
    # this is the process noise term
    v = np.sqrt(R)*np.random.normal(0,1, z.shape)
    
    z = (x*x)/20
    z= z+v
    
    return(z)

def nlikelihood(x, w, z, R = 1):
    '''
    % HDW 25/02/2003
    %
    % A function that computes a set of normalised weights for likely states
    % following an observation. The observation z is a fixed value.
    % The state x is a set of samples. The output is a set of normalised
    % weights nlw which encode the probability that the samples caused
    % the observation. This function encodes the sensor model
    '''
    
    # this is the sensor model, encoded as an innovation
    err = z-((x*x)/20) 
    
    # this is the assumed error on innovations
    # lower errors get higher weights
    nlw = np.exp(-(err*err)/(2*R))  
    
    # the following step is only necessary if we do not resample at every step
    nlw = nlw*w
    
    # normalise the weights
    nlw = nlw/np.sum(nlw)
    
    return(nlw)
    

def resample(xp,wp):
    '''
    % HDW 25/02/2003
    %
    % This is a function to do resampling of a sample set xp
    % according to the weighting vector wp. The result is a new 
    % sample set xe which has a uniform weighting vector we
    '''
    
    nw = len(wp)
    xe = np.zeros(xp.shape)
    
    
    # compute normalised CDF of current weights
    cdf = np.cumsum(wp)
    cdf = cdf/cdf[nw-1]
    
    # get a uniform sample of equivalent size and in sorted order
    u = sorteduniform(nw)
    #plt.plot(u, cdf)
    # now resample distribution
    j=0
    for i in range(0, nw):
       while cdf[j] < u[i]:     # if the weight is rising higher than uniform, keep resampling
          j = j+1
     
       xe[i] = xp[j]             # xp is the prediction
    
    # now we've resampled the high weights, the weights can be reset, equal to 1/n
    we= np.ones(wp.shape)/nw
    
    return(xe, we)

    
def sir(xinit, winit, zdata):
    
    """
    Implementation of the Sampling-Importance Resamping (SIR) algorithm
    HDW 25/02/2003, revised 1/03/2003
    
    x is a vector of state support values (particles)
    w are the weights on these particles 
    together x and w define a probability distribution
    
    The algorithm starts with an initial set of particles and weights (xinit,winit)
    A set of observations are provided
    The SIR loop proceeds as follows:
    		propogate particles through a process model (prediction)
     		update weights according to the likelihood function (observation)
    		depending on the particle distribution, resample (x,w)
    """
    
    # get sizes including number of simulation steps (N_STEPS) and particles (NUM_P)
    (NUM_STEPS, Z_DIM) = zdata.shape
    (NUM_P, X_DIM) = xinit.shape
    (temp1, temp2) = winit.shape
    if temp1 != NUM_P:
       raise Exception('Number of weights and Particles must be the same')
    
    # make space to record results
    xtrack = np.zeros((NUM_P, X_DIM, NUM_STEPS))
    wtrack = np.zeros((NUM_P, 1, NUM_STEPS))
    xintermed = np.zeros((NUM_P, X_DIM, NUM_STEPS))
    z_pred = np.zeros((NUM_P, X_DIM, NUM_STEPS))
    
    # initialise book keeping
    xtrack[:,:,0] = xinit	
    wtrack[:,:,0] = winit
    xintermed[:,:,0] = xinit
    z_pred[:,:,0] = xinit*xinit/20
    
    # now do main loop, note we skip the first observation
    for k in range(1,NUM_STEPS):
       x = prediction(xtrack[:,:, k-1], k) 				# propagate particles through process model
       xintermed[:,:,k] = x                             # save x before resampling to examine later
       z_pred[:,:,k] = x*x/20
       w = nlikelihood(x, wtrack[:,:, k-1], zdata[k,:]) 	# compute new weights from observation likelihood
       Neff = 1/sum(w*w)                                # compute effetive number of particles
       rho = Neff/NUM_P
       #plt.plot(u, np.cumsum(w))
       if rho < 0.5: 					# test for resampling - if effective no. samples is less than half of the original number of samples
          [x,w]=resample(x,w) 	# do resampling
          #x=regularise(x)        # optional regularisation...does not seem to make much difference
       
        #plt.hist(x, 40, facecolor='blue', alpha=0.5)
       xtrack[:,:,k] = x				# record results
       wtrack[:,:,k] = w
    
    return(xtrack, wtrack, xintermed, z_pred)

def create_plot_data(SIM_LENGTH, NUM_SAMP, xtrack, xintermed, z_pred):
    
    xstime= np.zeros((NUM_SAMP*SIM_LENGTH,1))
    pxsdata= np.zeros((NUM_SAMP*SIM_LENGTH,1))
    xsmean= np.zeros((1,SIM_LENGTH))
    xsintermed = np.zeros((NUM_SAMP*SIM_LENGTH,1))
    zspred = np.zeros((NUM_SAMP*SIM_LENGTH,1))
    
    for i in range(0, SIM_LENGTH):
       dstart= i*NUM_SAMP
       dend= (i+1)*NUM_SAMP
       xstime[dstart:dend, 0] = i
       pxsdata[dstart:dend, 0] = xtrack[:,0,i]
       xsintermed[dstart:dend, 0] = xintermed[:,0,i]
       zspred[dstart:dend, 0] = z_pred[:,0,i]
       xsmean[0][i] = np.mean(xtrack[:,0,i])
       
    return(xstime, pxsdata, xsmean, xsintermed, zspred)

def innovation(xintermed, zdata):
    sim_len = zdata.shape[0]
    innov = np.zeros((sim_len,1))
    
    for i in range(0,sim_len):
        mean_pred = np.mean((xintermed[:,0,i]*xintermed[:,0,i])/20)
        innov[i] = zdata[i,0] - mean_pred
    
    return(innov)


def auto(x):
    """
    Rewrite of Hugh Durrant-Whyte 5-Jan-94
    Computes autocorrelation of input data set. N is the number of 
    data points, x is a column matrix holding the input data set.
    Uses fft method as advertised in Maybeck p193.
    """

    # first find the size of input vector
    N = np.max(x.shape)
    M = int(round(N/2))

    # then compute PSD of data 
    X = np.fft.fft(x)
    # .* is element-wise multiplication
    Pxx = X * (np.conj(X)/N)

    # then inverse is autocorrelation
    Rxx = np.real(np.fft.ifft(Pxx))
    Rxx = Rxx[0:M]
    fact = Rxx[0]
    Rxx = Rxx/fact
                 
    return(Rxx)

